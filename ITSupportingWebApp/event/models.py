from djongo import models
from django.contrib.auth.models import User
from elearning.models import Course
from froala_editor.fields import FroalaField
from django.dispatch import receiver
from django.db.models.signals import m2m_changed

# Create your models here.

class Event(models.Model):
    title = models.CharField(max_length=250)
    content = FroalaField()
    ondate = models.DateField(blank=True, null=True)
    start_time = models.TimeField(blank=True, null=True)
    end_time = models.TimeField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name='events')
    join = models.BooleanField(default=False)
    attendee = models.ManyToManyField(User, related_name='attend_events',blank=True)

    def __str__(self):
        return self.title

class Notification(models.Model):
    receiver = models.ManyToManyField(User, through='Noti_Receiver')
    content = models.TextField(blank=True, null=True)
    link = models.URLField(max_length=1000,blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created'] 

class Noti_Receiver(models.Model):
    receiver = models.ForeignKey(User, on_delete=models.CASCADE, related_name='noti_receiver')
    notification = models.ForeignKey(Notification, on_delete=models.CASCADE, related_name='noti_receiver')
    is_read = models.BooleanField(default=False)