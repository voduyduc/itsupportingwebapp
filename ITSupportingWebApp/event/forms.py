from .models import Event
from django.forms import ModelForm
from django import forms
from account.widgets import DateTimePickerInput, TimePickerInput, DatePickerInput

class CreateEventForm(ModelForm):
    class Meta:
        model = Event
        fields = ['title','content','join','ondate','start_time','end_time']
    def __init__(self, *args, **kwargs):
        super(CreateEventForm, self).__init__(*args, **kwargs)
        self.fields['ondate'] = forms.DateField(input_formats=['%d/%m/%Y'],widget=DatePickerInput(),required=False)
        self.fields['start_time'] = forms.TimeField(input_formats=['%I:%M %p'],widget=TimePickerInput(),required=False)
        self.fields['end_time'] = forms.TimeField(input_formats=['%I:%M %p'],widget=TimePickerInput(),required=False)