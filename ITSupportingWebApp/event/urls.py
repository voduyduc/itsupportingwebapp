from django.urls import path, include
from . import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns=[
    path('',views.event_list,name='event_list'),
    path('create_event/',views.create_event,name='create_event'),
    path('<int:pk>/',views.event_detail,name='event_detail'),
    path('<int:pk>/edit',views.edit_event,name='edit_event'),
    path('<int:pk>/delete',views.delete_event,name='delete_event'),
    path('<int:pk>/join',views.join_event,name='join_event'),
    path('notis/',views.notis_json, name='notis.json'),
]

urlpatterns = format_suffix_patterns(urlpatterns)