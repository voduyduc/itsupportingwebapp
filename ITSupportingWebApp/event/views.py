from django.shortcuts import render, redirect, get_object_or_404
from .models import Event, Noti_Receiver, Notification
from .forms import CreateEventForm
from django.http import JsonResponse
import json
from django.utils import timezone
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import serializers
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator

# Create your views here.
@login_required
def create_event(request):
    if request.user.is_staff or request.user.is_superuser:
        events = Event.objects.all().order_by('-created')
        if request.method == 'POST':
            form = CreateEventForm(request.POST)
            if form.is_valid():
                new_event = form.save(commit=False)
                new_event.created_by = request.user
                new_event.save()
                return redirect('home')
        else:
            form = CreateEventForm()
        return render(request,'event/create_event.html',{'form':form,
                                                        'events':events,})
    else:
        return redirect('home')

def event_list(request):
    search = request.GET.get('search')
    all_events = Event.objects.all().order_by('-created')
    paginator = Paginator(all_events, 10)

    if search != "" and search != None:
        search_events = Event.objects.filter(title__icontains=search) or Event.objects.filter(content__icontains=search)
        search_events = search_events.order_by('-created')
        paginator = Paginator(search_events, 10)
    
    page = request.GET.get('page')
    events = paginator.get_page(page)
    return render(request,'event/event_list.html',{'all_events':all_events,
                                            'events':events, 
                                            'section':'event_list'})

def event_detail(request,pk):
    form = CreateEventForm()
    events = Event.objects.all().order_by('-created')
    event = get_object_or_404(Event, pk=pk)
    user_joined = False
    if request.user.is_authenticated:
        if event in request.user.attend_events.all():
            user_joined = True
    return render(request,'event/event_detail.html',{'form':form,
                                                    'events':events,
                                                    'event':event,
                                                    'user_joined':user_joined})

@login_required
def edit_event(request,pk):
    if request.user.is_staff or request.user.is_superuser:
        events = Event.objects.all().order_by('-created')
        event = get_object_or_404(Event, pk=pk)
        section = 'edit'
        if request.method == 'POST':
            form = CreateEventForm(request.POST,instance=event)
            if form.is_valid():
                form.save()
            return redirect('event_detail',pk)
        else:
            form = CreateEventForm(instance=event)
        return render(request,'event/create_event.html',{'events':events,
                                                        'event':event,
                                                        'section':section,
                                                        'form':form})
    else:
        return redirect('home')

@login_required
def delete_event(request,pk):
    if request.user.is_staff or request.user.is_superuser:
        event = get_object_or_404(Event, pk=pk)
        event.delete()
    return redirect('home')

@login_required
def join_event(request,pk):
    data = {} 
    event = get_object_or_404(Event, pk=pk)
    if request.is_ajax():
        if request.method == 'POST':
            if not request.user.attend_events.filter(pk=pk):
                event.attendee.add(request.user)
            data = {
                'join':"True",
            }
        if request.method == 'DELETE':
            if request.user.attend_events.filter(pk=pk):
                event.attendee.remove(request.user)
            data = {
                'join':"False",
            }
    return JsonResponse(data)

@api_view(['GET','POST'])
def notis_json(request):
    user = request.user
    data = []
    notis = Notification.objects.filter(receiver=user)
    for noti in notis:
        t = timezone.localtime(noti.created)
        data.append({
            'content': noti.content,
            'link' : noti.link,
            'created' : t.strftime('%H:%M %a %d/%m/%y'),
            'is_read' : noti.noti_receiver.get(receiver=user).is_read,
        })
    if request.method == 'GET':
        return Response(data)
    elif request.method == 'POST':
        for noti in notis:
            read = noti.noti_receiver.get(receiver=user)
            read.is_read = True
            read.save()
        return Response(data)
