from django import forms
from django.forms import ModelForm
from .models import Course, Course_Offering, Content, Semester, Major, CourseGrade, Assignment, Submission, EnrollDate, Discussion
from django.contrib.auth.models import User
from account.models import Profile
from account.widgets import DateTimePickerInput, TimePickerInput, DatePickerInput
from django.utils.dateformat import DateFormat, TimeFormat

class CourseForm(ModelForm): 
    requirements = forms.ModelMultipleChoiceField(queryset=Course.objects.all(),
                                                widget=forms.CheckboxSelectMultiple(),required=False)
    class Meta:
        model = Course
        fields = ['picture','title','department','required_credits','overview','num_credits','semester','category','major']

    def __init__(self, *args, **kwargs):
        super(CourseForm, self).__init__(*args, **kwargs)
        self.fields['major'] = forms.ModelMultipleChoiceField(queryset=Major.objects.all(),
                                                widget=forms.CheckboxSelectMultiple(),required=False)
        

class CourseOfferingForm(ModelForm):
    class Meta:
        model = Course_Offering
        fields = ['course','lecturer','num_students','semester','start_date','start_time','end_time']

    def __init__(self, *args, **kwargs):
        super(CourseOfferingForm, self).__init__(*args, **kwargs)
        users = User.objects.filter(is_active=True)
        username = []
        for user in users:
            if user.profile.role == 'professor':
                username.append(user.username)
        users = User.objects.filter(username__in=username)
        self.fields['lecturer'].queryset = users
        self.fields['start_date'] = forms.DateField(input_formats=['%d/%m/%Y'],widget=DatePickerInput())
        self.fields['start_time'] = forms.TimeField(input_formats=['%I:%M %p'],widget=TimePickerInput())
        self.fields['end_time'] = forms.TimeField(input_formats=['%I:%M %p'],widget=TimePickerInput())

class AddContentForm(ModelForm):
    image_field = forms.ImageField(required=False, widget=forms.ClearableFileInput(attrs={'multiple': True}))
    file_field = forms.FileField(required=False, widget=forms.ClearableFileInput(attrs={'multiple': True}))
    class Meta:
        model = Content
        fields = ['title','body']

class CreateSemesterForm(ModelForm):
    class Meta:
        model = Semester
        fields = ['start','end']

    def __init__(self, *args, **kwargs):
        super(CreateSemesterForm, self).__init__(*args, **kwargs)
        self.fields['start'] = forms.DateField(input_formats=['%d/%m/%Y'],widget=DatePickerInput())
        self.fields['end'] = forms.DateField(input_formats=['%d/%m/%Y'],widget=DatePickerInput())

class CreateMajorForm(ModelForm):
    class Meta:
        model = Major
        fields = ['name','content']

class InputGradeForm(ModelForm):
    class Meta:
        model = CourseGrade
        fields = ['assignment','midterm','final']

class CreateAssignment(ModelForm):

    class Meta:
        model = Assignment
        fields = ['title','body','deadline']

    def __init__(self, *args, **kwargs):
        super(CreateAssignment, self).__init__(*args, **kwargs)
        self.fields['deadline'] = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M'],widget=DateTimePickerInput())

class SubmissionForm(ModelForm):
    class Meta:
        model = Submission
        fields = ['File','body']

class EnrollDateForm(ModelForm):
    class Meta:
        model = EnrollDate
        fields = ['start_enroll','end_enroll']

    def __init__(self, *args, **kwargs):
        super(EnrollDateForm, self).__init__(*args, **kwargs)
        self.fields['start_enroll'] = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M'],widget=DateTimePickerInput())
        self.fields['end_enroll'] = forms.DateTimeField(input_formats=['%d/%m/%Y %H:%M'],widget=DateTimePickerInput())

class DiscussionForm(ModelForm):
    class Meta:
        model = Discussion
        fields = ['message']