from django.urls import path,include
from . import views
from rest_framework import routers

app_name = 'elearning'

router = routers.DefaultRouter()
router.register(r'majors', views.MajorViewSet)

urlpatterns = [
    path('program/',views.program,name='program'),
    path('course_offerings/<str:pk>/',views.course_offering_detail,name='course_offering_detail'),
    path('course_offerings/<str:pk>/staff_contact',views.owner_contact,name='owner_contact'),
    path('course_offerings/<str:pk>/discussion',views.discussion,name='discussion'),
    path('course_offerings/<str:pk>/add_content',views.add_content,name='add_content'),
    path('course_offerings/<str:pk>/contents/<str:content_pk>',views.content_detail,name='content_detail'),
    path('course_offerings/<str:pk>/contents/<str:content_pk>/edit',views.edit_content,name='edit_content'),
    path('course_offerings/<str:pk>/add_assignment',views.add_assignment,name='add_assignment'),
    path('course_offerings/<str:pk>/assignments',views.assignments,name='assignments'),
    path('course_offerings/<str:pk>/assignments/<int:assignment_pk>/',views.assignment_detail,name='assignment_detail'),
    path('course_offerings/<str:pk>/assignments/<int:assignment_pk>/submissions',views.submissions_json,name='submissions.json'),
    path('course_offerings/<str:pk>/assignments/<int:assignment_pk>/edit',views.edit_assignment,name='edit_assignment'),
    path('course_offerings/<str:pk>/assignments/<int:assignment_pk>/delete',views.delete_assignment,name='delete_assignment'),
    path('course_offerings/<str:pk>/grades/',views.grade_list,name='grade_list'),
    path('course_offerings/<str:pk>/grades/<str:username>',views.grade,name='grade'),
    path('majors/<str:major_slug>/',views.course_list,name='course_list'),
    path('courses/<str:pk>/',views.course_detail,name='course_detail'),
    path('enroll/',views.enroll,name='enroll'),
    path('enroll/course_offerings.json',views.course_offerings_json,name='course_offerings.json'),
    path('enroll/enroll.json',views.enroll_json,name='enroll.json'),
    path('', include(router.urls)),
]