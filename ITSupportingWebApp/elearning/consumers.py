import asyncio
import json
from django.contrib.auth.models import User
from channels.consumer import AsyncConsumer
from channels.db import database_sync_to_async

from .models import Course_Offering, Discussion

class DiscussionConsumer(AsyncConsumer):
    async def websocket_connect(self, event):
        print("connected", event)
        me = self.scope['user']
        offering_id = self.scope['url_route']['kwargs']['pk']
        self.course_offering = offering_id
        await self.channel_layer.group_add(
            offering_id,
            self.channel_name
        )
        await self.send({
            "type": "websocket.accept"
        })
        print(self.course_offering)
        
    async def websocket_receive(self, event):
        print("receive", event)
        front_text = event.get('text', None)
        if front_text is not None:
            loaded_dict_data = json.loads(front_text)
            msg = loaded_dict_data.get('message')
            user = self.scope['user']
            username = ''
            if user.is_authenticated:
                username = user.username
            myResponse = {
                'message':msg,
                'username':username
            }
            await self.create_chat_message(msg)
            await self.channel_layer.group_send(
                self.course_offering,
                {
                "type":"chat_message",
                "text":json.dumps(myResponse)
                }
            )
    
    async def chat_message(self, event):
        print('message', event)
        await self.send({
            "type":"websocket.send",
            "text": event['text']
        })

    async def websocket_disconnect(self, event):
        print("disconnected", event)
    
    @database_sync_to_async
    def create_chat_message(self, msg):
        user = self.scope['user']
        course_offering = Course_Offering.objects.get(pk=self.course_offering)
        return Discussion.objects.create(course_offering=course_offering,owner=user,message=msg)
