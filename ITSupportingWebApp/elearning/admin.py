from django.contrib import admin
from .models import Course, Course_Offering, Department
# Register your models here.

class DepartmentAdmin(admin.ModelAdmin):
    list_display=['name','department_id']

class CourseAdmin(admin.ModelAdmin):
    list_display=['title','course_id']

class CourseOfferingAmdin(admin.ModelAdmin):
    list_display=['offering_id','course','created','lecturer']
    list_filter=['offering_id','created']
    search_fields=['offering_id']

admin.site.register(Course_Offering, CourseOfferingAmdin)
admin.site.register(Course, CourseAdmin)
admin.site.register(Department, DepartmentAdmin)