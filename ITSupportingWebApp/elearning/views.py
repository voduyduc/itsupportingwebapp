from django.shortcuts import render, redirect, Http404, get_object_or_404
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required
from .models import Course, Course_Offering, Content, Content_File, Content_Image, Requirement, Major, Semester, CourseGrade, Assignment, Submission, EnrollDate, Discussion
from .forms import CourseForm, CourseOfferingForm, AddContentForm, CreateSemesterForm, InputGradeForm, CreateAssignment, SubmissionForm, DiscussionForm
from rest_framework import viewsets, serializers
import math
from event.models import Notification, Noti_Receiver, Event
from django.utils import timezone
import json
from django.utils.dateformat import DateFormat, TimeFormat
import os

# Create your views here.

def background_img():
    data = [
        {
            'url': 'static/account/assets/img/background.jpg'
        },
        {
            'url': 'static/img/background-2.jpeg'
        },
        {
            'url': 'static/img/background-3.jpg'
        },
        {
            'url': 'static/img/background-4.jpg',
        },
    ]
    return data

def home(request):
    bg_images = background_img()
    events = Event.objects.all().order_by('-created')
    majors = Major.objects.all()
    courses = Course.published.all()
    return render(request,'home.html',{'majors':majors,
                                    'courses':courses,
                                    'events':events,
                                    'bg_images': bg_images})

def about(request):
    List = ['Dean','Vice Dean']
    majors = Major.objects.all()
    for major in majors:
        List.append(major.name+' Professor')
    groups = Group.objects.filter(name__in=List)
    for group in groups:
        group_users = []
        for user in User.objects.filter(profile__role="professor"):
            if user.groups.filter(name=group.name):
                group_users.append(user)
        group.users = group_users
    return render(request,'elearning/about.html',{'groups':groups})    

def get_user(request):
    user = User.objects.get(username=request.user.username)
    return user

def program(request):
    majors = Major.objects.all()
    courses = Course.published.all()
    return render(request,'elearning/program.html',{'majors':majors,
                                                    'courses':courses})

def get_categories(major):
    courses = Course.objects.filter(major=major)
    credits = {
            'general courses': 0,
            'core courses': 0,
            'major courses': 0,
            'others': 0,
            'total': 0,
        }
    categories = []
    if courses:
        for course in courses:
            credits[course.category] += course.num_credits
            credits['total'] += course.num_credits
        for each in credits:
            if each == 'total':
                continue
            categories.append({
                'name': each,
                'credits': credits[each],
                'percent': str( round((credits[each]/credits['total'])*100,1) ) + '%',
            })
    else:
        for each in credits:
            if each == 'total':
                continue
            categories.append({
                'name': each,
                'credits': credits[each],
                'percent': '0.0%',
            })
    return categories

def get_mycourses(request):
    my_courses = ''
    if request.user.is_authenticated:
        if request.user.profile.role == "student":
            my_courses = request.user.course_grade.all()
        elif request.user.profile.role == "professor":
            my_courses = request.user.teaching_courses.all()
    return my_courses

def course_list(request, major_slug):
    major = Major.objects.get(pk=major_slug)
    categories = get_categories(major)
    courses = Course.objects.filter(major=major)
    semesters = range(1,9)
    my_courses = get_mycourses(request)
    return render(request,'elearning/course_list.html',{'my_courses':my_courses,
                                                    'categories':categories,
                                                    'semesters':semesters,
                                                    'courses':courses,
                                                    'major':major,})

def course_detail(request, pk):
    course = Course.published.get(pk=pk)
    now = timezone.now().date()
    semester = Semester.objects.all().order_by('-start')[0]
    course_offerings = course.course_offerings.filter(status__in=['published','started'],semester=semester)
    return render(request,'elearning/course_detail.html',{'course':course,'course_offerings':course_offerings})

def check_requirement(semester, course_offering, user):
    registered_courses = user.course_grade.filter(semester=semester)

    course_offering.registered = False
    for registered_course in registered_courses:
        if course_offering == registered_course.course_offering:
            course_offering.registered = True

    if (course_offering.course.semester %2 ) == 0:
        course_offering.year = (course_offering.course.semester//2)
    else:
        course_offering.year = (course_offering.course.semester//2)+1

    #check for requirement
    year_now = timezone.now().year
    student_year = year_now - user.entrance_student.latest('pk').year
    if semester.semester == 'semester 1':
        student_year = student_year + 1

    messages = []
    if student_year >= course_offering.year :
        course_offering.qualified = True
        requirements = course_offering.course.following_courses.all()
        
        for requirement in requirements:
            if not user.course_grade.filter(course=requirement.required_course).exclude(semester=semester):
                course_offering.qualified = False
                messages.append(requirement.required_course.title+" (not yet)")
        course_offering.messages = messages
    else:
        course_offering.qualified = False
        messages.append("You are in "+str(student_year)+" year")
        course_offering.messages = messages
    
    course_offering_json = {
        'course_id': course_offering.course.course_id,
        'offering_id': course_offering.pk,
        'course': course_offering.course.title,
        'lecturer': course_offering.lecturer.username,
        'year': course_offering.year,
        'start_date': DateFormat(course_offering.start_date).format('d/m/Y'),
        'day': DateFormat(course_offering.start_date).format('D'),
        'start_time': TimeFormat(course_offering.start_time).format('H:i'),
        'end_time': TimeFormat(course_offering.end_time).format('H:i'),
        'num_students': course_offering.num_students,
        'num_credits': course_offering.course.num_credits,
        'slot': course_offering.slot,
        'qualified': course_offering.qualified,
        'messages': course_offering.messages,
        'registered': course_offering.registered,
    }
    return course_offering_json

def course_offerings_json(request):
    try:
        user = request.user
        semester = Semester.objects.latest('start')
        course_offerings = Course_Offering.objects.filter(status__in=['published'],semester=semester).order_by("course")
        
        if request.is_ajax():
            if request.method == 'POST':
                json_data = request.read()
                data = json.loads(json_data)
                search = data['search']
                if search != "":
                    courses = Course.objects.filter(title__icontains=search)
                    course_offerings = Course_Offering.objects.filter(status='published',semester=semester,course__in=courses) | Course_Offering.objects.filter(status='published',semester=semester,offering_id__icontains=search)
        data = []
        for course_offering in course_offerings:
            course_offering_json = check_requirement(semester, course_offering, user)
            data.append(course_offering_json)
    except:
        print('Something went wrong during search.')
        data=[]
    return JsonResponse(data,safe=False)

def validate_enroll(request, course_offering):
    # check if user is busy or that course is already registered in that semester
    validate = None
    user = request.user
    semester = course_offering.semester
    message = None

    if course_offering.slot > 0:
        if semester.semester == 'semester 3':
            total_credits = 12
        else:
            total_credits = 24

        registered_courses = user.course_grade.filter(semester=semester)
        student_credits = 0

        for each in registered_courses:
            student_credits = student_credits + each.course.num_credits
        
        if( (student_credits + course_offering.course.num_credits) <= total_credits):
            #check if course_offering is already registered or not
            if not registered_courses.filter(course_offering=course_offering):
                #check if course is already registered in that semester
                for each in registered_courses:
                    if course_offering.course == each.course:
                        validate = False
                        message = "You have registered this course in this semester"
                    else:
                        if DateFormat(course_offering.start_date).format('D') == DateFormat(each.course_offering.start_date).format('D'):
                            if course_offering.start_time < each.course_offering.start_time:
                                if course_offering.end_time < each.course_offering.start_time:
                                    validate = True
                                else:
                                    validate = False
                            elif course_offering.start_time > each.course_offering.end_time:
                                validate = True
                            else:
                                validate = False
                            if validate == False:
                                message = "You have to study "+ each.course.title + " from "+TimeFormat(each.course_offering.start_time).format('H:i')+" to "+TimeFormat(each.course_offering.end_time).format('H:i')+" on "+DateFormat(each.course_offering.start_date).format('D')
            else:
                message = "You have registered this course_offering"
        else:
            message = "You have reached over 24 credits in this semester"
    else:
        message = "The course_offering is out of slot"
    return message

def enroll_json(request):
    user = request.user
    if request.is_ajax():

        json_data = request.read()
        data = json.loads(json_data)
        pk = data['pk']
        
        if request.method == 'POST':
            qualified = data['qualified']
            if pk:
                course_offering = get_object_or_404(Course_Offering, pk=pk)
                semester = course_offering.semester
                if qualified == True:
                    message = validate_enroll(request,course_offering)
                    if message == None:
                        CourseGrade.objects.create(course=course_offering.course,course_offering=course_offering,student=user)
                        
                        data = {
                            'messages': None,
                        }
                    else:
                        data = {
                            'messages': message,
                        } 
                elif qualified == False:
                    data = check_requirement(semester,course_offering,user)

        if request.method == 'DELETE':
            
            if pk:
                course_offering = get_object_or_404(Course_Offering, pk=pk)
                CourseGrade.objects.filter(course_offering=course_offering,student=user).delete()
                       
        return JsonResponse(data)

@login_required
def enroll(request):
    user = request.user
    semester = Semester.objects.latest('start')
    inenroll = False
    if user.profile.role == 'student':
        entrance = user.entrance_student.latest('pk')
        enroll_date = EnrollDate.objects.get(semester=semester,entrance=entrance)
        now = timezone.now()
        if enroll_date.start_enroll != None and enroll_date.end_enroll != None:
            if now >= enroll_date.start_enroll and now <= enroll_date.end_enroll:
                inenroll = True
    return render(request,'elearning/enroll.html',{'semester':semester,'inenroll':inenroll})

@login_required
def course_offering_detail(request, pk):
    user = get_user(request)
    course_offering = Course_Offering.objects.get(pk=pk)
    if course_offering.lecturer.username == user.username or course_offering.course_grade.filter(student=user):
        course = course_offering.course
        contents = course_offering.contents.all()
        return render(request,'elearning/course_offering_detail.html',{'course':course,'course_offering':course_offering,'contents':contents})
    else:
        raise Http404("You are not in the course_offering.")

@login_required
def add_content(request,pk):
    user = get_user(request)
    #try:    
    course_offering = user.teaching_courses.get(pk=pk, status__in=['published','started'])
    if request.method == "POST":
        form = AddContentForm(request.POST, request.FILES)
        images = request.FILES.getlist('image_field')
        files = request.FILES.getlist('file_field')

        if form.is_valid():

            new_content = form.save(commit=False)
            new_content.course_offering = course_offering
            new_content.save()
            content = Content.objects.get(pk=new_content.pk)
            for i in images:
                Content_Image.objects.create(content=content,image=i)
            for f in files:
                Content_File.objects.create(content=content,File=f)
            
        return redirect('elearning:course_offering_detail',course_offering.pk)
    
    else:
        form = AddContentForm()
    return render(request,'elearning/add_content.html',{'form':form,'course_offering':course_offering})
    
    #except:
        #raise Http404("You are not in the course_offering or do not have permission")

@login_required
def content_detail(request, pk, content_pk):
    user = get_user(request)
    course_offering = Course_Offering.objects.get(pk=pk)
    if course_offering.lecturer.username == user.username or course_offering.course_grade.filter(student=user):
        content = Content.objects.get(pk=content_pk)
        files = content.files.all()
        images = content.images.all()
        return render(request,'elearning/content_detail.html',{'content':content,'course_offering':course_offering,'files':files,'images':images})
    else:
        raise Http404("You are not in the course_offering.")

@login_required
def edit_content(request,pk, content_pk):
    user = get_user(request)
    course_offering = Course_Offering.objects.get(pk=pk)
    if course_offering.lecturer.username == user.username :
        content = Content.objects.get(pk=content_pk)
        if request.method == "POST":
            form = AddContentForm(request.POST, instance=content)
            content = form.save()
        else:
            form = AddContentForm(instance=content)
        return render(request,'elearning/edit_content.html',{'content':content,'course_offering':course_offering,'form':form})
    else:
        raise Http404("You are not in the course_offering.")

@login_required
def owner_contact(request,pk):
    user = get_user(request)
    course_offering = Course_Offering.objects.get(pk=pk)
    lecturer = course_offering.lecturer
    if course_offering.lecturer.username == user.username or course_offering.course_grade.filter(student=user):
        return render(request,'elearning/staff_contact.html',{'course_offering':course_offering,'lecturer':lecturer})
    else:
        raise Http404("You are not in the course_offering.")

@login_required
def grade_list(request,pk):
    user = get_user(request)
    course_offering = Course_Offering.objects.get(pk=pk)
    lecturer = course_offering.lecturer
    if lecturer == user:
        grades = CourseGrade.objects.filter(course_offering=course_offering)
        return render(request,'elearning/grade_list.html',{'course_offering':course_offering,'lecturer':lecturer,'grades':grades})
    else:
        raise Http404("You are not in the course_offering.")

@login_required
def grade(request,pk,username):
    user = User.objects.get(username=username)
    course_offering = Course_Offering.objects.get(pk=pk)
    lecturer = course_offering.lecturer
    if course_offering.lecturer.username == request.user.username or course_offering.course_grade.filter(student=user):
        grade = user.course_grade.get(course_offering=course_offering)
        if request.method == "POST":
            form = InputGradeForm(request.POST,instance=grade)
            if form.is_valid() and course_offering.lecturer == request.user:
                form.save()
                return redirect('elearning:grade_list',course_offering.pk)
        else:
            form = InputGradeForm(instance=grade)
        return render(request,'elearning/grade.html',{'course_offering':course_offering,'lecturer':lecturer,'form':form,'user':user,'grade':grade})
    else:
        raise Http404("You are not in the course_offering.")

@login_required
def add_assignment(request,pk):
    user = get_user(request)
    try:    
        course_offering = user.teaching_courses.get(pk=pk, status__in=['published','started'])
        if request.method == "POST":
            form = CreateAssignment(request.POST)

            if form.is_valid():
        
                new_assignment = form.save(commit=False)
                new_assignment.updated = timezone.now()
                new_assignment.course_offering = course_offering
                new_assignment.created_by = user
                new_assignment.save()
                
            return redirect('elearning:assignments',course_offering.pk)
        
        else:
            form = CreateAssignment()
        return render(request,'elearning/add_assignment.html',{'form':form,'course_offering':course_offering})
    
    except:
        raise Http404("You are not in the course_offering or do not have permission")

@login_required
def edit_assignment(request,pk, assignment_pk):
    user = get_user(request)
    try:    
        course_offering = user.teaching_courses.get(pk=pk)
        assignment = get_object_or_404(Assignment,pk=assignment_pk)
        if request.method == "POST":
            form = CreateAssignment(request.POST, instance=assignment)
            if form.is_valid():
                assignment = form.save(commit=False)
                assignment.updated = timezone.now()
                assignment.save()
            return redirect('elearning:assignments',course_offering.pk)
        
        else:
            form = CreateAssignment(instance=assignment)
        return render(request,'elearning/add_assignment.html',{'form':form,'course_offering':course_offering,'assignment':assignment,'section':'edit'})
    
    except:
        raise Http404("You are not in the course_offering or do not have permission")

@login_required
def delete_assignment(request,pk,assignment_pk):
    user = get_user(request)
    try:    
        course_offering = user.teaching_courses.get(pk=pk, status__in=['published','started'])
        assignment = get_object_or_404(Assignment,pk=assignment_pk)
        assignment.delete()
        return redirect('elearning:assignments',course_offering.pk)
    except:
        raise Http404("You are not in the course_offering or do not have permission")

@login_required
def assignments(request, pk):
    user = get_user(request)
    course_offering = Course_Offering.objects.get(pk=pk)
    if course_offering.lecturer.username == user.username or course_offering.course_grade.filter(student=user):
        assignments = Assignment.objects.filter(course_offering=course_offering).order_by('-updated')
        for assignment in assignments:
            if assignment.submissions.filter(student=user):
                assignment.is_submitted = True
            else:
                assignment.is_submitted = False
        return render(request,'elearning/assignments.html',{'assignments':assignments,'course_offering':course_offering})
    else:
        raise Http404("You are not in the course_offering.")

@login_required
def assignment_detail(request, pk, assignment_pk):
    user = get_user(request)
    course_offering = Course_Offering.objects.get(pk=pk)
    if course_offering.lecturer.username == user.username or course_offering.course_grade.filter(student=user):
        assignment = get_object_or_404(Assignment,pk=assignment_pk)
        submissions = assignment.submissions.filter(student=user)
        students = course_offering.course_grade.all()

        if course_offering.lecturer == user:
            
            for each in students:
                student_submissions = assignment.submissions.filter(student=each.student)
                if student_submissions:

                    if student_submissions.latest('submitted').submitted > assignment.deadline:
                        each.status = "Late"
                    elif student_submissions.latest('submitted').submitted <= assignment.deadline:
                        each.status = "On time"

                else:
                    each.status = "Not yet"

        if submissions:
            assignment.is_submitted = True
        else:
            assignment.is_submitted = False
            if request.method == 'POST':
                form = SubmissionForm(request.POST,request.FILES)
                if form.is_valid():
                    new_submission = form.save(commit=False)
                    new_submission.assignment = assignment
                    new_submission.student = request.user
                    new_submission.save()
                return redirect ('elearning:assignment_detail',course_offering.pk,assignment.pk)
        form = SubmissionForm()
        return render(request,'elearning/assignment_detail.html',{'assignment':assignment,'course_offering':course_offering,'submissions':submissions,'form':form,'students':students})
    else:
        raise Http404("You are not in the course_offering.")

@login_required
def submissions_json(request, pk, assignment_pk):
    assignment = get_object_or_404(Assignment,pk=assignment_pk)
    if request.is_ajax():
        if request.method == 'POST':
            json_data = request.read()
            data = json.loads(json_data)
            student = get_object_or_404(User,username=data['username'])
            submissions = assignment.submissions.filter(student=student)
            data = []
            for submission in submissions:
                submitted = submission.submitted.astimezone(timezone.get_default_timezone())
                File = str(submission.File)
                filename = os.path.basename(submission.File.name)
                data.append({
                    'File' : File,
                    'filename' : filename,
                    'body' : submission.body,
                    'submitted': submitted.strftime('%d/%m/%Y %H:%M'),
                })
            print(data)
            return HttpResponse(json.dumps(data),
                            content_type='application/json')

@login_required
def discussion(request,pk):            
    user = get_user(request)
    course_offering = Course_Offering.objects.get(pk=pk)
    if course_offering.lecturer == user or course_offering.course_grade.filter(student=user):
        people = []
        people.append({
            'username':course_offering.lecturer.username,
            'role':course_offering.lecturer.profile.role,
            'avatar':course_offering.lecturer.profile.avatar,
        })
        for each in course_offering.course_grade.all():
            people.append({
                'username':each.student.username,
                'role':each.student.profile.role,
                'avatar':each.student.profile.avatar,        
            })
        discussions = course_offering.discussion.all()
        if request.method == 'POST':
            form = DiscussionForm(request.POST)
            if form.is_valid:
                new_message = form.save(commit=False)
                new_message.owner = user
                new_message.course_offering = course_offering
                new_message.save()
        else:
            form = DiscussionForm()
        return render(request,'elearning/discussion.html',{'course_offering':course_offering,'form':form,'people':people,'discussions':discussions})
    else:
        raise Http404("You are not in the course_offering.")

#serialize Major model
class MajorListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Major
        fields = ('name','slug')

#view major only
class MajorViewSet(viewsets.ModelViewSet):
    queryset = Major.objects.all()
    serializer_class = MajorListSerializer 