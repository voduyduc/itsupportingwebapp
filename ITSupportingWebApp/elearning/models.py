from djongo import models
from django.contrib.auth.models import User
from account.models import Profile
from django.utils import timezone
from django.utils.text import slugify
from django.dispatch import receiver
from froala_editor.fields import FroalaField
import os
from django.core.exceptions import ValidationError
from django.core.validators import EmailValidator, MaxValueValidator, MinValueValidator
from django.db.models.signals import post_save, post_delete
from account.models import Entrance
from django.utils.dateformat import DateFormat, TimeFormat
import re
from account.models import Entrance

# Create your models here.

class PublishedManager(models.Manager):
    def get_queryset(self):
       return super(PublishedManager,
                    self).get_queryset()\
                        .filter(status='published')

class PendingManager(models.Manager):
    def get_queryset(self):
       return super(PendingManager,
                    self).get_queryset()\
                        .filter(status='pending')

class Department(models.Model):
    name = models.CharField(max_length=200)
    department_id = models.CharField(primary_key=True, unique=True, max_length=200)

    def __str__(self):
        return self.name

class Major(models.Model):
    department = models.ForeignKey(Department, on_delete=models.CASCADE, related_name='majors')
    name = models.CharField(max_length=200)
    slug = models.SlugField(primary_key=True, unique=True)
    content = models.TextField(blank=True, null=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Major, self).save(*args, **kwargs)

    class Meta:
        ordering = ['name']
    
    def __str__(self):
        return self.name

def course_image_path(instance, filename):
    return 'courses/{0}/images/{1}'.format(instance.pk, filename)

class Course(models.Model):

    STATUS_CHOICES = (
        ('published','Published'),
        ('pending','Pending'),
        ('reject','Reject'),
    )

    CATEGORY_CHOICES = (
        ('general courses','General Courses'),
        ('core courses','Core Courses'),
        ('major courses','Major Courses'),
        ('others', 'Others'),
    )

    SEMESTER_CHOICES = (
        (1,'1'),
        (2,'2'),
        (3,'3'),
        (4,'4'),
        (5,'5'),
        (6,'6'),
        (7,'7'),
        (8,'8'),
    )
    
    picture = models.ImageField(upload_to=course_image_path,blank=True, null=True,max_length=1000)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, related_name='courses')
    title = models.CharField(max_length=200)
    course_id = models.SlugField(primary_key=True, unique=True)
    required_credits = models.IntegerField(blank=True, null=True)
    overview = models.TextField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, related_name="courses", blank=True, null=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES)
    num_credits = models.IntegerField(blank=True, null=True)
    category = models.CharField(max_length=20, choices=CATEGORY_CHOICES, default='others')
    semester = models.IntegerField(choices=SEMESTER_CHOICES,blank=True, null=True)
    major = models.ManyToManyField(Major, related_name="courses", blank=True)

    def clean(self):
        if self.course_id is None or self.course_id == "":
            slug = slugify(self.title)
            courses = Course.objects.all()
            for course in courses:
                if slug == slugify(course.title):
                    print("Duplicate title")
                    raise ValidationError('Duplicate title')

    def save(self, *args, **kwargs):
        try:
            self.clean()
            if self.course_id is None or self.course_id == "":
                courses = Course.objects.filter(department=self.department)
                new_id = ''
                if courses:
                    latest_course = courses.latest('created')
                    digit = re.findall("[0-9]",latest_course.course_id)
                    new_id = int(new_id.join(digit)) + 1
                else:
                    new_id = 1 
                self.course_id = self.department.department_id + str(new_id).zfill(3) + 'IU'
            super(Course, self).save(*args, **kwargs)
        except:
            print("Duplicate title")

    class Meta:
        ordering = ['title']
    
    def __str__(self):
        return self.title

    objects = models.Manager() # The default manager.
    published = PublishedManager() # Our custom manager.
    pending = PendingManager()

@receiver(models.signals.post_delete, sender=Course)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.picture:
        if os.path.isfile(instance.picture.path):
            os.remove(instance.picture.path)

@receiver(models.signals.pre_save, sender=Course)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """
    Deletes old file from filesystem
    when corresponding `MediaFile` object is updated
    with new file.
    """
    if not instance.pk:
        return False

    try:
        old_file = sender.objects.get(pk=instance.pk).picture
    except sender.DoesNotExist:
        return False

    new_file = instance.picture
    if old_file:
        if not old_file == new_file:
            if os.path.isfile(old_file.path):
                os.remove(old_file.path)

class Requirement(models.Model):
    following_course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='following_courses')#MH theo sau
    required_course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='required_courses')#MH tien quyet

class Semester(models.Model):

    SEMESTER_CHOICES = (
        ('semester 1','Semester 1'),
        ('semester 2','Semester 2'),
        ('semester 3','Semester 3'),
    )

    slug = models.SlugField(primary_key=True, unique=True)
    semester = models.CharField(max_length=10, choices=SEMESTER_CHOICES)
    start = models.DateField()
    end = models.DateField()

    def clean(self):
        if not (self.start or self.end) :
            raise ValidationError('Not valid.')
        if self.start > self.end:
            raise ValidationError('End date has to be after Start date.')
        if self.start.year == self.end.year: # semester 2 & 3
            print('ok')
            if self.end.month - self.start.month < 2:
                print("error1")
                raise ValidationError('Semester has to be at least 3 months.')
            elif self.end.month - self.start.month > 4:
                print("error2")
                raise ValidationError('Semester has to be at most 4 months.')
        elif self.end.year - self.start.year == 1: # semester 1
            if ((12 - self.start.month) + self.end.month) < 3:
                print("error3")
                raise ValidationError('Semester has to be at least 3 months.')
            elif ((12 - self.start.month) + self.end.month) > 4:
                print("error4")
                raise ValidationError('Semester has to be at most 4 months.')

    def save(self, *args, **kwargs):
        start_month = self.start.month
        year = self.start.year
        
        if start_month >= 9: # semester 1 start at September
            self.semester = 'semester 1'
            slug = self.semester + ' ' + str(year) + ' ' + str(year+1)
        elif start_month <= 3: # semester 2 start at February
            self.semester = 'semester 2'
            slug = self.semester + ' ' + str(year-1) + ' ' + str(year)
        else: # semester 3 start at June or July
            self.semester = 'semester 3'
            slug = self.semester + ' ' + str(year-1) + ' ' + str(year)
        print(slug)
        self.slug = slugify(slug)
        super(Semester, self).save(*args, **kwargs)

    def __str__(self):
        return self.slug

class Course_Offering(models.Model):

    STATUS_CHOICES = (
        ('published','Published'),
        ('pending','Pending'),
        ('reject','Reject'),
        ('started','Started'),
        ('completed','Completed'),
    )
    
    lecturer = models.ForeignKey(User, on_delete=models.CASCADE, related_name='teaching_courses')
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='course_offerings')
    semester = models.ForeignKey(Semester, on_delete=models.CASCADE, related_name='course_offerings')
    offering_id = models.SlugField(max_length=200,primary_key= True,unique= True)
    start_date = models.DateField(blank=True, null=True)
    start_time = models.TimeField(blank=True, null=True)
    end_time = models.TimeField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=10,choices=STATUS_CHOICES)
    num_students = models.IntegerField(blank=True, null=True)
    slot = models.IntegerField(blank=True, null=True)
    
    def clean(self):
        if not (self.start_time or self.end_time) :
            raise ValidationError('Not valid.')
        if self.start_time > self.end_time:
            raise ValidationError('End time has to be after Start time.')
        if self.offering_id is None or self.offering_id == "":
            #check schedule of the lecturer
            course_offerings = Course_Offering.objects.filter(semester=self.semester, lecturer=self.lecturer)
            for offering in course_offerings:
                if DateFormat(self.start_date).format('D') == DateFormat(offering.start_date).format('D'):
                    if self.start_time < offering.start_time:
                        if self.end_time < offering.start_time:
                            validate = True
                        else:
                            validate = False
                    elif self.start_time > offering.end_time:
                        validate = True
                    else:
                        validate = False
                    if validate == False:
                        message = self.lecturer.username + " have to teach "+ offering.course.title + " from "+TimeFormat(offering.start_time).format('H:i')+" to "+TimeFormat(offering.end_time).format('H:i')+" on "+DateFormat(offering.start_date).format('D')
                        print("time error")
                        raise ValidationError(message)

    def save(self, *args, **kwargs):
        self.slot = self.num_students - self.course_grade.count()
        if self.offering_id is None or self.offering_id == "":
            course_offerings = Course_Offering.objects.filter(course=self.course)
            new_id = ''
            if course_offerings:
                latest_course_offering = course_offerings.latest('created')
                latest_id = latest_course_offering.offering_id.split("IU")[-1]
                new_id = int(latest_id) + 1
            else:
                new_id = 1 
            self.offering_id = self.course.course_id + str(new_id)
        super(Course_Offering, self).save(*args, **kwargs)

    class Meta:
        ordering=['-created']

    def __str__(self):
        return self.offering_id

    objects = models.Manager() # The default manager.
    published = PublishedManager() # Our custom manager.
    pending = PendingManager()

class EnrollDate(models.Model):
    semester = models.ForeignKey(Semester, on_delete=models.CASCADE, related_name="enroll_date")
    entrance = models.ForeignKey(Entrance, on_delete=models.CASCADE, related_name="enroll_date")
    start_enroll = models.DateTimeField(blank=True, null=True)
    end_enroll = models.DateTimeField(blank=True, null=True)

@receiver(post_save, sender=Entrance)
def create_enroll_date_by_entrance(sender, instance, **kwargs):
    semesters = Semester.objects.all()
    for semester in semesters:
        if not EnrollDate.objects.filter(semester=semester,entrance=instance):
            EnrollDate.objects.create(semester=semester,entrance=instance)

@receiver(post_save, sender=Semester)
def create_enroll_date(sender, instance, **kwargs):
    entrances = Entrance.objects.all()
    for entrance in entrances:
        if not EnrollDate.objects.filter(semester=instance,entrance=entrance):
            EnrollDate.objects.create(semester=instance,entrance=entrance)

class Content(models.Model):
    course_offering = models.ForeignKey(Course_Offering, on_delete=models.CASCADE, related_name="contents")
    title = models.CharField(max_length=200)
    body = FroalaField(blank=True, null=True)
    updated = models.DateTimeField()
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="contents")

    class Meta:
        ordering = ['created']
    
    def __str__(self):
        return self.title

def content_image_path(instance, filename):
    course_offering = instance.content.course_offering
    return 'courses/{0}/semesters/{1}/{2}/contents/{3}/images/{4}'.format(course_offering.course.pk,course_offering.semester.pk, course_offering.pk, instance.content.pk, filename)

class Content_Image(models.Model):
    content = models.ForeignKey(Content, on_delete=models.CASCADE, related_name="images")
    image = models.ImageField(upload_to=content_image_path,max_length=1000)
    uploaded = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['uploaded']
    
    def filename(self):
        return os.path.basename(self.image.name)


def content_file_path(instance, filename):
    course_offering = instance.content.course_offering
    return 'courses/{0}/semesters/{1}/{2}/contents/{3}/files/{4}'.format(course_offering.course.pk,course_offering.semester.pk, course_offering.pk, instance.content.pk, filename)

class Content_File(models.Model):
    content = models.ForeignKey(Content, on_delete=models.CASCADE, related_name="files")
    File = models.FileField(upload_to=content_file_path,max_length=1000)
    uploaded = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['uploaded']
    
    def filename(self):
        return os.path.basename(self.File.name)

class Assignment(models.Model):
    course_offering = models.ForeignKey(Course_Offering, on_delete=models.CASCADE, related_name='assignments')
    title = models.CharField(max_length=200)
    body = FroalaField(blank=True, null=True)
    updated = models.DateTimeField()
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, related_name="assignments")
    deadline = models.DateTimeField()
    due = models.DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ['deadline']
    
    def __str__(self):
        return self.title

def submission_file_path(instance, filename):
    course_offering = instance.assignment.course_offering
    return 'courses/{0}/semesters/{1}/{2}/assignments/{3}/submissions/{4}'.format(course_offering.course.pk,course_offering.semester.pk, course_offering.pk, instance.assignment.pk, filename)

class Submission(models.Model):
    assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE, related_name='submissions')
    File = models.FileField(upload_to=submission_file_path, blank=True, null=True, max_length=1000)
    body = FroalaField(blank=True, null=True)
    student = models.ForeignKey(User, on_delete=models.CASCADE, related_name='submissions')
    submitted = models.DateTimeField(auto_now_add=True)

    def filename(self):
        return os.path.basename(self.File.name)

class CourseGrade(models.Model):
    semester = models.ForeignKey(Semester, on_delete=models.CASCADE, related_name='course_grade',blank=True, null=True)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='course_grade')
    course_offering = models.ForeignKey(Course_Offering, on_delete=models.CASCADE, related_name='course_grade')
    student = models.ForeignKey(User, on_delete=models.CASCADE, related_name='course_grade')
    assignment = models.IntegerField(blank=True, null=True, validators=[MinValueValidator(0),MaxValueValidator(100)])
    midterm = models.IntegerField(blank=True, null=True, validators=[MinValueValidator(0),MaxValueValidator(100)])
    final = models.IntegerField(blank=True, null=True, validators=[MinValueValidator(0),MaxValueValidator(100)])
    average = models.IntegerField(blank=True, null=True, validators=[MinValueValidator(0),MaxValueValidator(100)])
    status = models.CharField(max_length=20,blank=True, null=True)
    
    def save(self, *args, **kwargs):
        self.semester = self.course_offering.semester
        self.course = self.course_offering.course
        if self.assignment and self.midterm and self.final:
            self.average = self.assignment*0.3 + self.midterm*0.3 + self.final*0.4
            if self.average >= 50:
                self.status = 'passed'
            else:
                self.status = 'failed'
        super(CourseGrade, self).save(*args, **kwargs)

@receiver(post_delete, sender=CourseGrade)
@receiver(post_save, sender=CourseGrade)
def update_slot(sender, instance, **kwargs):
    instance.course_offering.save()

class Discussion(models.Model):
    course_offering = models.ForeignKey(Course_Offering, on_delete=models.CASCADE, related_name='discussion')
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='discussion')
    sent = models.DateTimeField(auto_now_add=True)
    message = models.TextField()

    class Meta:
       ordering = ['sent']