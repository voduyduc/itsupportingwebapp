from django import forms
from .models import Entrance
from django.contrib.auth.models import User
import datetime

class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

def current_year():
    return datetime.date.today().year
    
def year_choices():
    return [(r,r) for r in range(datetime.date.today().year-10, datetime.date.today().year+1)]

class CreateEntranceForm(forms.ModelForm):
    
    class Meta:
        model = Entrance
        fields = ['year','advisor']

    def __init__(self, *args, **kwargs):
        super(CreateEntranceForm, self).__init__(*args, **kwargs)
        users = User.objects.filter(is_active=True)
        username = []
        for user in users:
            if user.profile.role == 'professor':
                username.append(user.username)
        users = User.objects.filter(username__in=username)
        self.fields['advisor'].queryset = users
        self.fields['year'] = forms.TypedChoiceField(coerce=int, choices=year_choices, initial=current_year)