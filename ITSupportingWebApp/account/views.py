from django.shortcuts import render,redirect, get_object_or_404
from django.http import HttpResponse, JsonResponse, Http404
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from .forms import LoginForm, CreateEntranceForm
from django.contrib.auth.models import User, Permission, Group
from .models import Entrance
import json
from django.core.files.storage import FileSystemStorage
from xlrd import open_workbook

from django.utils import timezone
from forum.models import Post
from forum.models import Category as Post_Category
from elearning.models import Course, Course_Offering, Requirement, Major, Semester, EnrollDate, Department, CourseGrade
from event.models import Event

from elearning.forms import CourseForm, CourseOfferingForm, CreateMajorForm, CreateSemesterForm, EnrollDateForm
from forum.forms import CategoryForm
from ITSupportingWebApp.settings import BASE_DIR
from django.utils.text import slugify
from django.utils.dateformat import DateFormat, TimeFormat
from datetime import timedelta

# Create your views here.

def get_user(request):
    user = User.objects.get(username=request.user.username)
    return user

def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            username = cd['username']
            password = cd['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponse('Authenticated Successfully')
                else: 
                    return HttpResponse('Disable user')
        else:
            return HttpResponse('Invalid Login')
    else:
        form = LoginForm()
    return render(request, 'account/login.html', {'form':form})

@login_required
def dashboard(request):
    return render(request, 'account/dashboard.html',{'section':'dashboard'})

@login_required
def profile(request, username):
    user = User.objects.get(username=username)
    section = None
    if request.user == user:
        section = 'profile'
    perms = user.user_permissions.all()
    return render(request,'account/profile.html',{'user':user,'perms':perms,'section':section})

@login_required
def edit_profile(request,username):
    user = User.objects.get(username=username)
    if request.user.username == user.username:
        if request.method == 'POST':
            user.email = request.POST.get('email')
            user.first_name = request.POST.get('first_name')
            user.last_name = request.POST.get('last_name')
            birth_date = request.POST.get('birth_date')
            if birth_date:
                user.profile.birth_date = birth_date
            user.profile.major = request.POST.get('major')
            user.profile.bio = request.POST.get('bio')
            user.save()
            return redirect('profile',username=user.username)
        return render(request,'account/profile.html',{'user':user,'section':'edit_profile'})
    else:
        return redirect('profile',username=user.username)

@login_required
def edit_avatar(request,username):
    user = User.objects.get(username=username)
    if request.user == user:
        if request.method == 'POST':
            user.profile.avatar = request.FILES.get('avatar')
            user.save()
        return redirect('edit_profile',username=user.username)
    else:
        return redirect('profile',username=user.username)

def user_by_role(role):
    users = User.objects.all()
    username = []
    for user in users:
        if user.profile.role == role:
            username.append(user.username)
    user = User.objects.filter(username__in=username)
    return user

def add_user(request,role):
    if request.method == 'POST':
        username = request.POST.get('username')
        email = request.POST.get('email')
        password = request.POST.get('password')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        user = authenticate(username=username,password=password)
        if user is not None:
            print('User has already existed')
        else:
            user = User.objects.create_user(username=username,email=email,password=password)
            user.first_name = first_name
            user.last_name = last_name
            user.profile.role = role
            if role == 'staff':
                user.is_staff = True
            birth_date = request.POST.get('birth_date')
            if birth_date:
                user.profile.birth_date = birth_date
            user.profile.major = request.POST.get('major')
            user.profile.bio = request.POST.get('bio')
            user.save()
        return user                

@login_required
def manage_user(request, role):
    if request.user.is_superuser or request.user.is_staff:
        if role == 'staff' or role == 'professor':
            groups = Group.objects.all()
            new_user = add_user(request,role)
            users = user_by_role(role)
            section = 'manage_'+role
            return render(request,'account/manage_user.html',{'users':users,'groups':groups,'role':role,'section':section})
        
        elif role == 'student':
            form = CreateEntranceForm()
            entrances = Entrance.objects.all()
            return render(request,'account/manage_entrance.html',{'entrances':entrances,
                                                                'form':form,
                                                                'section':'manage_student'})
        
        else:
            raise Http404('Role does not exist')
    else:
        raise Http404("You do not have permission.")

@login_required
def add_entrance(request):
    if request.user.is_superuser or request.user.is_staff:
        if request.method == "POST":
            form = CreateEntranceForm(request.POST)
            if form.is_valid:
                form.save()
    return redirect('manage_user','student')

@login_required
def manage_student(request, year):
    if request.user.is_superuser or request.user.is_staff:
        entrance = Entrance.objects.get(year=year)
        role = 'student'
        if entrance:
            groups = Group.objects.all()
            new_user = add_user(request,role)
            entrance.student.add(new_user)
            users = entrance.student.all()
            return render(request,'account/manage_user.html',{'users':users,
                                                            'groups':groups,
                                                            'role':role,
                                                            'entrance':entrance,
                                                            'section':'manage_student'})
        
        else:
            raise Http404('Role does not exist')
    else:
        raise Http404("You do not have permission.")

@login_required
def user_json(request):
    if request.user.is_superuser or request.user.is_staff:
        try:
            if request.is_ajax():
                json_data = request.read()
                data = json.loads(json_data)
                username = data['username']
                user = get_object_or_404(User, username=username)
                groups = user.groups.all()

                if request.method == "POST":
                    
                    data = []
                    if groups:
                        for group in groups:
                            data.append({
                                'name': group.name,
                            })
                        
                    return HttpResponse(json.dumps(data),
                                        content_type='application/json')

                elif request.method == "PUT":
                    edited_groups = data['groups']
                    if edited_groups:                        
                        if groups:
                            for edited_group in edited_groups:
                                edited_group = Group.objects.get(name=edited_group)
                                if edited_group not in groups:
                                    user.groups.add(edited_group)
                            for group in groups:
                                if group.name not in edited_groups:
                                    user.groups.remove(group)
                            
                        else:
                            for edited_group in edited_groups:
                                edited_group = Group.objects.get(name=edited_group)
                                user.groups.add(edited_group)                             
                    else:
                        for group in groups:
                            user.groups.remove(group)
                    
                    groups = user.groups.all()
                    user.user_permissions.clear()

                    if groups:
                        for group in groups:
                            perms = group.permissions.all()
                            user_perms = user.user_permissions.all()
                            if perms:
                                for perm in perms:
                                    if perm not in user_perms:
                                        
                                        user.user_permissions.add(perm)

                    return JsonResponse(data)

                elif request.method == "DELETE":
                    user.delete()
                    return JsonResponse(data)
        except:
            return HttpResponse('error')

@login_required
def add_group(request):
    if request.user.is_superuser or request.user.is_staff:
        if request.method == "POST":
            group_name = request.POST.get('group_name')
            perms = request.POST.getlist('checks')
            group = Group.objects.create(name=group_name)
            for perm in perms:
                perm = Permission.objects.get(codename=perm)
                group.permissions.add(perm)
            group.save()
        return redirect('manage_group') 
    else:
        raise Http404("You do not have permission.")

@login_required
def edit_group(request):
    if request.user.is_superuser or request.user.is_staff:
        try:
            if request.is_ajax():
                if request.method == "POST":
                    json_data = request.read()
                    data = json.loads(json_data)
                    group_name = data['name']
                    print(group_name)
                    if group_name:
                        group = Group.objects.get(name=group_name)
                        perms = group.permissions.all()

                        perm_list = []

                        for perm in perms:
                            perm_list.append({
                                'name': perm.name,
                                'codename': perm.codename
                            })
                    return HttpResponse(json.dumps(perm_list),
                                        content_type='application/json')
                
                elif request.method == "PUT":
                    json_data = request.read()
                    data = json.loads(json_data)
                    group_name = data['name']
                    edited_perms = data['perms']
                    print(edited_perms)

                    if group_name:
                        group = Group.objects.get(name=group_name)
                        perms = group.permissions.all()

                    if edited_perms:                        
                        if perms:
                            for edited_perm in edited_perms:
                                edited_perm = Permission.objects.get(codename=edited_perm)
                                if edited_perm not in perms:
                                    group.permissions.add(edited_perm)
                            for perm in perms:
                                if perm.codename not in edited_perms:
                                    group.permissions.remove(perm)
                            
                        else:
                            for edited_perm in edited_perms:
                                edited_perm = Permission.objects.get(codename=edited_perm)
                                group.permissions.add(edited_perm)                             
                    else:
                        for perm in perms:
                            group.permissions.remove(perm)

                    return JsonResponse(data)

                if request.method == "DELETE":
                    json_data = request.read()
                    data = json.loads(json_data)
                    group_name = data['name']
                    print(group_name)
                    if group_name:
                        group = Group.objects.get(name=group_name)
                        group.delete()
                    return JsonResponse(data)
        except:
            return HttpResponse('error')
        
@login_required
def manage_group(request):
    if request.user.is_superuser or request.user.is_staff:
        users = User.objects.all()
        groups = Group.objects.all()
        permissions = Permission.objects.all()
        if request.method == "POST":
            group_name = request.POST.get('group_name')
            perms = request.POST.getlist('checks')
            group = Group.objects.create(name=group_name)
            for perm in perms:
                perm = Permission.objects.get(codename=perm)
                group.permissions.add(perm)
            group.save()
        return render(request,'account/manage_group.html',{'users':users,'groups':groups,'permissions':permissions,'section':'manage_group'})
    else:
        raise Http404("You do not have permission.")

@login_required
def manage_post(request):
    if request.user.is_superuser or request.user.is_staff:
        posts = Post.objects.all()
        categories = Post_Category.objects.all()
        if request.method == "POST":
            category_form = CategoryForm(request.POST)
            if category_form.is_valid():
                category_form.save()
        else:
            category_form = CategoryForm()
        return render(request,'account/manage_post.html',{'posts':posts,
                                                        'categories':categories,
                                                        'category_form':category_form,
                                                        'section':'manage_post'})
    else:
        raise Http404("You do not have permission.")

@login_required
def post_category_json(request):
    if request.is_ajax():
        json_data = request.read()
        data = json.loads(json_data)
        category_slug = data['slug']
        print(category_slug)
        if category_slug:
            category = get_object_or_404(Post_Category, slug=category_slug)
            if request.method =="DELETE":
                data = {
                    'slug':category.slug
                }
                category.delete()
        return JsonResponse(data)

@login_required
def post_json(request):
    if request.is_ajax():
        if request.method =="PUT":
            json_data = request.read()
            data = json.loads(json_data)
            post_id = data['id']
            if post_id:
                post = get_object_or_404(Post, id=post_id)
                post.status = "published"
                post.published = timezone.now()
                post.save()

                data = {
                    'id':post.id,
                    'title':post.title,
                    'body':post.body,
                    'status':post.status,
                }       

        elif request.method == "GET":
            post_id = str(request.GET.get('id'))
            if post_id:
                post = get_object_or_404(Post, id=post_id)
                
                data = {
                    'id':post.id,
                    'title':post.title,
                    'body':post.body,
                }

        elif request.method == "DELETE":
            json_data = request.read()
            data = json.loads(json_data)
            post_id = data['id']
            if post_id:
                post = get_object_or_404(Post, id=post_id)

                data = {
                    'id':post.id,
                }    
                post.delete()   

    return JsonResponse(data)

@login_required
def manage_course(request):
    if request.user.is_superuser or request.user.is_staff:
        majors = Major.objects.all()
        courses = Course.objects.all()
        if request.method == "POST":
            course_form = CourseForm(request.POST, request.FILES)
            major_form = CreateMajorForm(request.POST)    
            if course_form.is_valid():
                new_course = course_form.save(commit=False)
                new_course.status = "published"
                new_course.created_by = get_user(request)
                new_course.save()
                course_form.save_m2m()
                
                for requirement in request.POST.getlist('requirements'):
                    required_course = Course.objects.get(pk=requirement)
                    Requirement.objects.create(following_course=new_course,required_course=required_course)
            if major_form.is_valid():
                new_major = major_form.save()
            return redirect('manage_course')
        else:
            course_form = CourseForm()
            major_form = CreateMajorForm()
        return render(request,'account/manage_course.html',{'majors':majors,
                                                            'courses':courses,
                                                            'course_form':course_form,
                                                            'major_form':major_form,
                                                            'section':'manage_course'})
    else:
        raise Http404("You do not have permission.")

@login_required
def major_json(request):
    if request.is_ajax():
        json_data = request.read()
        data = json.loads(json_data)
        major_slug = data['pk']
        if major_slug:
            major = get_object_or_404(Major, pk=major_slug)
            if request.method =="DELETE":
                data = {
                    'pk':major.pk
                }
                major.delete()
        return JsonResponse(data)

@login_required
def course_json(request):
    if request.is_ajax():
        if request.method =="PUT":
            json_data = request.read()
            data = json.loads(json_data)
            course_slug = data['pk']
            print(course_slug)
            action = data['action']
            print(action)
            if course_slug:
                course = get_object_or_404(Course, pk=course_slug)
                if action == "done":
                    course.status = "published"
                elif action == "close":
                    course.status = "reject" 
                course.save()

                data = {
                    'pk':course.pk,
                    'title':course.title,
                    'overview':course.overview,
                    'status':course.status,
                }       

            return JsonResponse(data)

        elif request.method == "GET":
            course_slug = str(request.GET.get('pk'))
            if course_slug:
                course = get_object_or_404(Course, pk=course_slug)
                
                data = {
                    'pk':course.pk,
                    'title':course.title,
                    'overview':course.overview,
                }

            return JsonResponse(data)

        elif request.method == "DELETE":
            json_data = request.read()
            data = json.loads(json_data)
            course_slug = data['pk']
            if course_slug:
                course = get_object_or_404(Course, pk=course_slug)

                data = {
                    'pk':course.pk,
                }    
                course.delete()   

            return JsonResponse(data)

@login_required
def edit_course(request, pk):
    if request.user.is_superuser or request.user.is_staff:
        course = Course.objects.get(pk = pk)
        if request.method == "POST":
            form = CourseForm(request.POST, request.FILES,instance=course)
            if form.is_valid():
                form.save()
                #add requirements
                requirements = request.POST.getlist('requirements')
                for requirement in requirements:
                    required_course = Course.objects.get(pk=requirement)
                    old_requirements = Requirement.objects.filter(following_course=course,required_course=required_course)
                    if not old_requirements:
                        Requirement.objects.create(following_course=course,required_course=required_course)
                #remove requirements
                old_requirements =  Requirement.objects.filter(following_course=course)
                for old_requirement in old_requirements:
                    if old_requirement.required_course.pk not in requirements:
                        old_requirement.delete()
            return redirect('manage_course')
        else:
            List = []
            for r in course.following_courses.all():
                List.append(r.required_course.pk)
            required_courses = Course.objects.filter(pk__in=List)
            form = CourseForm(instance=course, initial={'requirements':required_courses})
        return render(request,'account/edit_course.html',{'course':course,
                                                        'form':form,
                                                        'section':'manage_course'})
    else:
        return redirect('dashboard')

@login_required
def manage_course_offering(request):
    if request.user.is_superuser or request.user.is_staff:
        semesters = Semester.objects.all().order_by('-start')
        course_offerings = Course_Offering.objects.all().order_by("-created")
        if request.method == "POST":
            form = CourseOfferingForm(request.POST)
            semester_form = CreateSemesterForm(request.POST)
            if form.is_valid():
                new_course_offering = form.save(commit=False)
                new_course_offering.status = "published"
                new_course_offering.save()
            if semester_form.is_valid():
                new_semester = semester_form.save()
            return redirect('manage_course_offering')
        else:
            semester_form = CreateSemesterForm()
            form = CourseOfferingForm()
        return render(request,'account/manage_course_offering.html',{'semesters':semesters,'course_offerings':course_offerings,'semester_form':semester_form,'form':form,'section':'manage_course_offering'})
    else:
        raise Http404("You do not have permission.")

@login_required
def course_by_semester(request,slug):
    if request.user.is_superuser or request.user.is_staff:
        semester = get_object_or_404(Semester,slug=slug)
        course_offerings = Course_Offering.objects.filter(semester=semester)
        if request.method == "POST":
            form = CourseOfferingForm(request.POST)
            if form.is_valid():
                new_course_offering = form.save(commit=False)
                new_course_offering.status = "published"
                new_course_offering.save()
            return redirect('course_by_semester',semester.pk)
        else:
            form = CourseOfferingForm()
        return render(request,'account/manage_course_offering.html',{'semester':semester,'course_offerings':course_offerings,'form':form,'section':'manage_course_offering'})
    else:
        raise Http404("You do not have permission.")

@login_required
def course_offering_json(request):
    if request.is_ajax():
        if request.method =="PUT":
            json_data = request.read()
            data = json.loads(json_data)
            pk = data['pk']
            action = data['action']
            if pk:
                course_offering = get_object_or_404(Course_Offering, pk=pk)
                if action == "done":
                    course_offering.status = "published"
                elif action == "close":
                    course_offering.status = "reject" 
                course_offering.save()

                data = {
                    'pk':course_offering.pk,
                    'offering_id':course_offering.offering_id,
                    'status':course_offering.status,
                }     

            return JsonResponse(data)

        elif request.method == "DELETE":
            json_data = request.read()
            data = json.loads(json_data)
            pk = data['pk']
            if pk:
                course_offering = get_object_or_404(Course_Offering, pk=pk)

                data = {
                    'pk':course_offering.pk,
                }    
                course_offering.delete()   

            return JsonResponse(data)

@login_required
def readfile(request):
    if request.user.is_superuser or request.user.is_staff:
        new_courses = []
        if request.method == "POST":
            f = request.FILES["load"]
            fs = FileSystemStorage()
            filename = fs.save(f.name, f)
            uploaded_file_url = fs.url(filename)
            wb = open_workbook(BASE_DIR + uploaded_file_url)
            for s in wb.sheets():
                for row in range(1, s.nrows):
                    col_names = s.row(0)
                    values = []
                    for name, col in zip(col_names, range(s.ncols)):
                        value  = (s.cell(row,col).value)
                        try : value = str(int(value))
                        except : pass
                        values.append(value)
                    new_courses.append({
                        'picture': '',
                        'title': values[0],
                        'department': values[1],
                        'majors': values[2],
                        'required': values[3],
                        'created_by': request.user,
                        'status': 'published',
                        'num_credits': values[4],
                        'category': values[5],
                        'semester': values[6],
                        'overview': values[7],
                    })
            fs.delete(filename)
            for new_course in new_courses:
                department = get_object_or_404(Department, department_id=new_course['department'])
                new = Course(title=new_course['title'],
                            department=department,
                            created_by=new_course['created_by'],
                            status=new_course['status'],
                            num_credits=new_course['num_credits'],
                            category=new_course['category'],
                            semester=new_course['semester'],
                            overview=new_course['overview'])
                new.save()
                if new_course['majors']:
                    majors = new_course['majors'].split(',')
                    for major in majors:
                        slug = slugify(major)
                        major = Major.objects.filter(slug=slug)
                        if major:
                            major = Major.objects.get(slug=slug)
                            new.major.add(major)

                if new_course['required']:
                    requirements = new_course['required'].split(',')
                    for requirement in requirements:
                        course = Course.objects.filter(title=requirement)
                        if course:
                            course = Course.objects.get(title=requirement)
                            Requirement.objects.create(following_course=new,required_course=course)
                
    return redirect('manage_course')

@login_required
def edit_course_offering(request, pk):
    if request.user.is_superuser or request.user.is_staff:
        entrances = Entrance.objects.all()
        course_offering = Course_Offering.objects.get(pk = pk)
        grades = course_offering.course_grade.all()
        for grade in grades:
            grade.student.entrance = grade.student.entrance_student.last()
    
        if request.method == "POST":
            form = CourseOfferingForm(request.POST, instance=course_offering)
            if form.is_valid():
                form.save()
            return redirect('edit_course_offering', course_offering.pk)
        else:
            form = CourseOfferingForm(instance=course_offering)
        return render(request,'account/edit_course_offering.html',{'course_offering':course_offering,
                                                                    'form':form,
                                                                    'grades':grades,
                                                                    'entrances':entrances,
                                                                    'section':'manage_course_offering'})
    else:
        raise Http404("You do not have permission.")

def validate_add(student, course_offering):
    student_grades = student.course_grade.filter(semester=course_offering.semester)
    message = None
    validate = None
                
    if course_offering.semester.semester == 'semester 3':
        total_credits = 12
    else:
        total_credits = 24

    student_credits = 0
    for grade in student_grades:
        student_credits = student_credits + grade.course.num_credits

    if student_credits + course_offering.course.num_credits > total_credits:
        message = student.username +' has registered '+ str(student_credits) + ' credits in this semester.'
    else:
        #check date
        for each in student_grades:
            if course_offering.course == each.course:
                validate = False
                message = student.username + " have registered this course in this semester"
            else:    
                if DateFormat(course_offering.start_date).format('D') == DateFormat(each.course_offering.start_date).format('D'):
                    if course_offering.start_time < each.course_offering.start_time:
                        if course_offering.end_time < each.course_offering.start_time:
                            validate = True
                        else:
                            validate = False
                    elif course_offering.start_time > each.course_offering.end_time:
                        validate = True
                    else:
                        validate = False
                    if validate == False:
                        message = "You have to study "+ each.course.title + " from "+TimeFormat(each.course_offering.start_time).format('H:i')+" to "+TimeFormat(each.course_offering.end_time).format('H:i')+" on "+DateFormat(each.course_offering.start_date).format('D')
                        break
    return message

@login_required
def student_json(request, pk):
    if request.is_ajax():
        course_offering = get_object_or_404(Course_Offering, pk=pk)
        data = None
        if request.method == "GET":
            grades = course_offering.course_grade.all()
            List = []
            for grade in grades:
                List.append(grade.student.username)

            entrances = Entrance.objects.all()
            students = []
            for entrance in entrances:
                if grades:
                    entrance_students = entrance.student.all().exclude(username__in=List)
                else:
                    entrance_students = entrance.student.all()
                for student in entrance_students:
                    students.append({
                        'username':student.username,
                        'first_name':student.first_name,
                        'last_name':student.last_name,
                        'year':str(entrance.year),
                    })
            data = students
            
        elif request.method == "POST":
            json_data = request.read()
            data = json.loads(json_data)
            username = data['username']
            if username:
                student = get_object_or_404(User, username=username)
                message = validate_add(student, course_offering)

                if message == None:
                    new_grade = CourseGrade(student=student,course_offering=course_offering)
                    new_grade.save()
                    count = course_offering.course_grade.all().count()
                    
                    data = {
                        'message':None,
                        'no.':count,
                        'username': student.username,
                        'first_name':student.first_name,
                        'last_name':student.last_name,
                        'year':str(student.entrance_student.last().year),
                    }
                else:
                    data = {
                        'message':message,
                    }


        elif request.method == "DELETE":
            json_data = request.read()
            data = json.loads(json_data)
            username = data['username']
            if username:
                student = get_object_or_404(User, username=username)
                grade = get_object_or_404(CourseGrade, student=student, course_offering=course_offering)
                grade.delete()
                
                data = {
                    'username': student.username,
                }
            
        return JsonResponse(data,safe=False)

@login_required
def update_course_status(request):
    if request.user.is_superuser or request.user.is_staff:
        course_offerings = Course_Offering.published.all()
        for course_offering in course_offerings:
            date = timezone.datetime.now().date()
            if date >= course_offering.start_date:
                course_offering.status = 'started'
                course_offering.save()                 
        return redirect('manage_course_offering')
    else:
        raise Http404("You do not have permission.")

@login_required
def my_courses(request):
    my_courses = ''
    pending_courses = ''
    form = None
    if request.user.profile.role == "student":
        my_courses = request.user.course_grade.all()
    elif request.user.profile.role == "professor":
        my_courses = request.user.teaching_courses.filter(status='published')
        pending_courses = request.user.teaching_courses.filter(status='pending')
        if request.method == "POST":
            form = CourseOfferingForm(request.POST)
            if form.is_valid():
                new_course_offering = form.save(commit=False)
                new_course_offering.lecturer = request.user
                new_course_offering.status = 'pending'
                new_course_offering.save()
        else:
            form = CourseOfferingForm()
    sems = []
    for my_course in my_courses:
        sem = my_course.semester.slug
        sems.append(sem)
    semesters = Semester.objects.filter(slug__in=sems).order_by('start')
    return render(request,'account/my_courses.html',{'semesters':semesters,
                                                    'my_courses':my_courses,
                                                    'pending_courses':pending_courses,
                                                    'form':form,
                                                    'section':'my_courses'})

@login_required
def cancel_request(request, pk):
    if request.user.profile.role == "professor":
        course_offering = get_object_or_404(Course_Offering,pk=pk, status="pending")
        if request.user == course_offering.lecturer:
            course_offering.delete()
            return redirect('my_courses')
        else:
            raise Http404("The course offering is not yours.")
    else:
        raise Http404("You do not have permission.")

@login_required
def semester_json(request):
    if request.is_ajax():
        if request.method == "DELETE":
            json_data = request.read()
            data = json.loads(json_data)
            pk = data['pk']
            if pk:
                semester = get_object_or_404(Semester, pk=pk)

                data = {
                    'pk':semester.pk,
                }    
                semester.delete()   

            return JsonResponse(data)

@login_required
def edit_semester(request, slug):
    if request.user.is_superuser or request.user.is_staff:
        semester = get_object_or_404(Semester,slug=slug)
        entrances = Entrance.objects.order_by('-year')[:4]

        for entrance in entrances:
            enroll_date = get_object_or_404(EnrollDate,entrance=entrance,semester=semester)
            entrance.start_enroll = enroll_date.start_enroll
            entrance.end_enroll = enroll_date.end_enroll

        if request.method == 'POST':
            semester_form = CreateSemesterForm(request.POST,instance=semester)
            if semester_form.is_valid():
                semester_form.save()
                return redirect('edit_semester',semester.slug)
        else:
            semester_form = CreateSemesterForm(instance=semester)
        form = EnrollDateForm()
        return render(request,'account/edit_semester.html',{'semester':semester,'semester_form':semester_form,'form':form,'entrances':entrances,'section':'manage_course_offering'})

@login_required
def edit_enroll_date(request, slug, pk):
    if request.user.is_superuser or request.user.is_staff:
        semester = get_object_or_404(Semester,slug=slug)
        entrances = Entrance.objects.order_by('-year')[3:]
        entrance = get_object_or_404(Entrance,pk=pk)
        multiple_enroll_date = None

        if entrances:
            if entrance == entrances[0]:
                multiple_enroll_date = semester.enroll_date.filter(entrance__in=entrances)

        enroll_date = get_object_or_404(EnrollDate,semester=semester,entrance=entrance)
        
        if request.method == 'POST':
            if multiple_enroll_date:
                for each in multiple_enroll_date:
                    form = EnrollDateForm(request.POST,instance=each)
                    form.save()
            else:
                form = EnrollDateForm(request.POST,instance=enroll_date)
                form.save()
        return redirect('edit_semester',semester.slug)

@login_required
def manage_event(request):
    if request.user.is_superuser or request.user.is_staff:
        events = Event.objects.all().order_by('-created')
        return render(request,'account/manage_event.html',{'events':events,
                                                        'section':'manage_event'})
    else:
        raise Http404("You do not have permission.")

@login_required
def event_join_list(request, pk):
    if request.user.is_superuser or request.user.is_staff:
        event = get_object_or_404(Event, pk=pk)
        attendees = event.attendee.all()
        return render(request,'account/event_join_list.html',{'event':event,
                                                        'attendees':attendees,
                                                        'section':'manage_event'})
    else:
        raise Http404("You do not have permission.")

@login_required
def delete_event(request, pk):
    if request.user.is_superuser or request.user.is_staff:
        event = get_object_or_404(Event, pk=pk)
        event.delete()
        return redirect('manage_event')
    else:
        raise Http404("You do not have permission.")

@login_required
def calendar(request):
    return render(request,'account/calendar.html',{'section':'calendar'})

@login_required
def calendar_json(request):
    event_list = []
    user = request.user

    if not user.profile.role =='staff':

        date = timezone.localdate()
        semesters = Semester.objects.all() 
        semester = None
        course_offerings = None

        for i in semesters:
            if i.start <= date and i.end >= date:
                semester = i
                break

        if user.profile.role == 'student':
            grades = request.user.course_grade.all()
            offering_ids = []
            for grade in grades:
                offering_ids.append(grade.course_offering.offering_id)
            course_offerings = Course_Offering.objects.filter(offering_id__in=offering_ids)
            #course_offerings = request.user.course_grade.all()#filter(semester=semester)
        elif user.profile.role == 'professor':
            course_offerings = request.user.teaching_courses.filter(semester=semester)

        for course_offering in course_offerings:
            monday1 = course_offering.semester.start - timedelta(days=course_offering.semester.start.weekday())
            monday2 = course_offering.semester.end - timedelta(days=course_offering.semester.end.weekday())
            weeks = int((monday2 - monday1).days / 7)

            for i in range(0,weeks-1):
                start_date = DateFormat(course_offering.start_date + timedelta(days=7*i)).format('Y-m-d')
                start_time = TimeFormat(course_offering.start_time).format('H:i')
                end_time = TimeFormat(course_offering.end_time).format('H:i')
                start = start_date +' '+ start_time
                end = start_date +' '+ end_time
            
                event_list.append({
                    'start': start,
                    'end': end,
                    'title': course_offering.course.title + '\n(' +course_offering.offering_id + ')',
                    'allDay': False,
                    'className': 'event-azure',
                })

    events = user.attend_events.all()

    for event in events:

        ondate = DateFormat(event.ondate).format('Y-m-d')
        start_time = TimeFormat(event.start_time).format('H:i')
        end_time = TimeFormat(event.end_time).format('H:i')
        start = ondate +' '+ start_time
        end = ondate +' '+ end_time

        event_list.append({
            'start': start,
            'end': end,
            'title': event.title,
            'allDay': False,
            'className': 'event-green',
        })

    return JsonResponse(event_list,safe=False)