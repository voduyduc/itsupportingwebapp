from djongo import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.validators import MaxValueValidator, MinValueValidator
import datetime
import os

# Create your models here.

def avatar_path(instance, filename):
    return 'users/{0}/avatar/{1}'.format(instance.user.username, filename)

class Profile(models.Model):

    ROLE_CHOICES = (
        ('staff','Staff'),
        ('professor','Professor'),
        ('student','Student'),        
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE,related_name='profile')
    role = models.CharField(max_length=20,choices=ROLE_CHOICES, blank=True, null=True)
    bio = models.TextField(max_length=500,blank=True)
    major = models.CharField(max_length=250,blank=True)
    birth_date = models.DateField(null=True,blank=True)
    avatar = models.ImageField(upload_to=avatar_path,blank=True, null=True,max_length=1000)

@receiver(models.signals.post_delete, sender=Profile)
def auto_delete_file_on_delete(sender, instance, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.avatar:
        if os.path.isfile(instance.avatar.path):
            os.remove(instance.avatar.path)

@receiver(models.signals.pre_save, sender=Profile)
def auto_delete_file_on_change(sender, instance, **kwargs):
    """
    Deletes old file from filesystem
    when corresponding `MediaFile` object is updated
    with new file.
    """
    if not instance.pk:
        return False

    try:
        old_file = sender.objects.get(pk=instance.pk).avatar
    except sender.DoesNotExist:
        return False

    new_file = instance.avatar
    if old_file:
        if not old_file == new_file:
            if os.path.isfile(old_file.path):
                os.remove(old_file.path)   

@receiver(post_save, sender=User)
def create_or_update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()

def current_year():
    return datetime.date.today().year

def max_value_current_year(value):
    return MaxValueValidator(current_year())(value)

class Entrance(models.Model):
    year = models.IntegerField(primary_key=True, 
                            validators=[MinValueValidator(2000),max_value_current_year])
    advisor = models.ForeignKey(User, on_delete=models.PROTECT, related_name='entrance_advisor')
    student = models.ManyToManyField(User, related_name='entrance_student', blank=True)

    def __str__(self):
        return str(self.year)