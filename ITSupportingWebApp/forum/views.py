from django.shortcuts import render, get_object_or_404,HttpResponse, redirect
from .models import Post,Comment,Category
from .forms import PostForm,CommentForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User, Permission
from django.utils import timesince
import json
from django.core.paginator import Paginator

# Create your views here.

def get_user(request):
    user = User.objects.get(username=request.user.username)
    return user

def category_list(request):
    categories = Category.objects.all()
    category_list = []
    all_posts = Post.published.order_by('-publish')[:3]
    for category in categories:
        try:
            latest_post = category.posts.filter(status='published').latest('publish')
            id = latest_post.id
            title = latest_post.title
            published = latest_post.publish
            author = latest_post.author
            category_list.append({
                'name': category.name,
                'slug': category.slug,
                'total_posts': category.posts.count(),
                'latest_post_id': id,
                'latest_post_title':title,
                'latest_post_published':published,
                'author': author,
            })
        except:
            category_list.append({
                'name': category.name,
                'slug': category.slug,
                'total_posts': 0,
            })
    return render(request,'forum/category.html',{'categories':category_list,'all_posts':all_posts})

def post_list(request, category_slug):
    categories = Category.objects.all()
    category = Category.objects.get(slug=category_slug)
    all_posts = category.posts.filter(status='published').order_by('-publish')
    for post in all_posts:
        post.num_comment = post.comments.count()
        comments = Comment.objects.filter(post=post)
        if comments:
            latest_comment = comments.latest('created')
            post.latest_comment = latest_comment.body
            post.latest_comment_author = latest_comment.author
            post.latest_comment_created = latest_comment.created

    paginator = Paginator(all_posts, 10)
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    return render(request,'forum/post_list.html',{'ctg':category,
                                            'categories':categories, 
                                            'all_posts':all_posts,
                                            'posts':posts, 
                                            'section':'post_list'})

def search_post(request):
    search = request.GET.get('search')
    categories = Category.objects.all()
    all_posts = Post.objects.filter(status='published').order_by('-publish')
    search_posts = Post.objects.filter(status='published',title__icontains=search) or Post.objects.filter(status='published',body__icontains=search)
    search_posts = search_posts.order_by('-publish')
    for post in search_posts:
        post.num_comment = post.comments.count()
        comments = Comment.objects.filter(post=post)
        if comments:
            latest_comment = comments.latest('created')
            post.latest_comment = latest_comment.body
            post.latest_comment_author = latest_comment.author
            post.latest_comment_created = latest_comment.created

    paginator = Paginator(search_posts, 10)
    page = request.GET.get('page')
    posts = paginator.get_page(page)
    return render(request,'forum/search_post_list.html',{'search':search,
                                            'categories':categories,
                                            'all_posts':all_posts,
                                            'posts':posts, 
                                            'section':'post_list'})

def post_detail(request, category_slug, post_id):
    form = PostForm()
    comment_form = CommentForm()

    category = Category.objects.get(slug=category_slug)
    posts = category.posts.filter(status='published')
    categories = Category.objects.all()
    post = get_object_or_404(Post, id=post_id, 
                                status="published",)
    comments = post.comments.all()

    if request.method=="GET":
        post.views = post.views + 1
        post.save()
    
    if request.user.is_authenticated:
        if request.user in post.like.all():
            post.is_liked = True
        for comment in comments:
            if request.user in comment.like.all():
                comment.is_liked = True
        if request.method=="POST":
            comment_form = CommentForm(request.POST)
            if comment_form.is_valid():
                new_comment = comment_form.save(commit=False)
                new_comment.post = post
                new_comment.author = get_user(request)
                new_comment.save()
            return redirect('forum:post_detail',category_slug,post_id)
        
    return render(request, 'forum/post_detail.html',{'ctg':category,
                                            'categories':categories,
                                            'posts':posts,
                                            'post':post,
                                            'comments':comments,
                                            'comment_form':comment_form,
                                            'form':form})

@login_required
def create_post(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            new_post = form.save(commit=False)
            new_post.author = get_user(request)
            if new_post.author.is_staff | new_post.author.has_perm('forum.add_post'):
                new_post.status = "published"
                new_post.save()
                category_slug = new_post.category.slug
                return redirect('forum:post_detail', category_slug=category_slug, post_id=str(new_post.id))
            else:
                new_post.status = "pending"
                new_post.save()
                return redirect('forum:category')
    else:
        form = PostForm()
    return render(request, 'forum/create_post.html',{'form':form})

@login_required
def delete_post(request, category_slug, post_id):
    post = get_object_or_404(Post,id=post_id)
    if post.author.username == get_user(request).username:
        post.delete()
    else:
        return "Do not have permission"
    return redirect('forum:post_list',category_slug=category_slug)

@login_required
def delete_comment(request, category_slug, post_id, comment_id):
    comment = get_object_or_404(Comment, id=comment_id)
    if comment.author.username == get_user(request).username:
        comment.delete()
    else:
        return "Do not have permission"
    return redirect('forum:post_detail',category_slug=category_slug,post_id=str(post_id))

@login_required
def edit_post(request, category_slug, post_id):
    category = Category.objects.get(slug=category_slug)
    post = get_object_or_404(Post,id=post_id)
    form = PostForm()
    if post.author.username == get_user(request).username:
        if request.method == 'POST':
            post.body = request.POST.get('body')
            post.save()
            return redirect('forum:post_detail',category_slug=category_slug,post_id=str(post_id))
    else:
        return "Do not have permission"
    return render(request,'forum/edit_post.html',{'ctg':category,'post':post,'form':form})

@login_required
def like_post(request, category_slug, post_id):
    if request.is_ajax():
        json_data = request.read()
        json_data = json.loads(json_data)
        username = json_data['username']
        post_id = json_data['post_id']
        user = User.objects.get(username=username)
        post = Post.objects.get(id=post_id)
        if request.method == "POST":
            post.like.add(user)
            liked_users = post.like.all()
        elif request.method == "DELETE":
            post.like.remove(user)
            liked_users = post.like.all()
        data = []
        for liked_user in liked_users:
            data.append({
                'name': liked_user.username,
            })
        print(data)
        return HttpResponse(json.dumps(data),
                            content_type='application/json')

@login_required
def like_comment(request, category_slug, post_id):
    if request.is_ajax():
        json_data = request.read()
        json_data = json.loads(json_data)
        username = json_data['username']
        comment_id = json_data['comment_id']
        user = User.objects.get(username=username)
        comment = Comment.objects.get(id=comment_id)
        
        if request.method == "POST":
            comment.like.add(user)    
        elif request.method == "DELETE":
            comment.like.remove(user)

        liked_users = comment.like.all()
        data = []
        for liked_user in liked_users:
            data.append({
                'name': liked_user.username,
            })
        print(data)
        return HttpResponse(json.dumps(data),
                            content_type='application/json')