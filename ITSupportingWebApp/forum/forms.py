from .models import Post, Comment, Category
from django.forms import ModelForm

class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ['title','category','body']

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = ['body']

class CategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = ['name']