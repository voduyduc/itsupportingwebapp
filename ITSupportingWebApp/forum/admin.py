from django.contrib import admin
from .models import Post, Comment, Category
# Register your models here.

class CommentInLine(admin.TabularInline):
    model=Comment
    extra = 1

class PostAdmin(admin.ModelAdmin):
    inlines = [CommentInLine]
    list_display=('title','publish','status','author')
    list_filter=['publish','title']
    search_fields = ['title']

admin.site.register(Post,PostAdmin)

class CategoryAdmin(admin.ModelAdmin):
    list_display=['name']

admin.site.register(Category,CategoryAdmin)
