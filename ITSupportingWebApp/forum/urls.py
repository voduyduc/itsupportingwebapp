from django.urls import path
from . import views

app_name = 'forum'

urlpatterns = [
    path('',views.category_list,name='category'),
    path('posts/',views.search_post,name='search_post'),
    path('<str:category_slug>/post/',views.post_list,name='post_list'),
    path('<str:category_slug>/post/<str:post_id>',views.post_detail,name='post_detail'),
    path('create_post/',views.create_post,name='create_post'),
    path('<str:category_slug>/post/<str:post_id>/delete',views.delete_post,name='delete_post'),
    path('<str:category_slug>/post/<str:post_id>/<str:comment_id>/delete',views.delete_comment,name='delete_comment'),
    path('<str:category_slug>/post/<str:post_id>/edit',views.edit_post,name='edit_post'),
    path('<str:category_slug>/post/<str:post_id>/like_post',views.like_post,name='like_post'),
    path('<str:category_slug>/post/<str:post_id>/like_comment',views.like_comment,name='like_comment'),
]