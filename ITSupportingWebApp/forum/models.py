from djongo import models
from django.utils import timezone
from django.contrib.auth.models import User
from froala_editor.fields import FroalaField
from django.urls import reverse
import uuid
from django.utils.text import slugify

# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=250)
    slug = models.SlugField(primary_key=True, unique=True)
    
    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)

class PublishedManager(models.Manager):
    def get_queryset(self):
        return super(PublishedManager,
                    self).get_queryset()\
                        .filter(status='published')

class Post(models.Model):
    STATUS_CHOICES = (
        ('draft','Draft'),
        ('published','Published'),
        ('pending','Pending'),
        ('reject','Reject'),
    )
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='posts')
    title = models.CharField(max_length=250)
    author = models.ForeignKey(User, on_delete=models.CASCADE,related_name='posts')
    body = FroalaField()
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=10,choices=STATUS_CHOICES,default='draft')
    views = models.IntegerField(default=0)
    like = models.ManyToManyField(User,related_name='liked_posts',blank=True)
    dislike = models.ManyToManyField(User,related_name='disliked_posts',blank=True)

    class meta:
        ordering = ('-publish',)

    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse('forum:post_detail',
                        args=[str(self.category.slug),str(self.id)])

    objects = models.Manager() # The default manager.
    published = PublishedManager() # Our custom manager.

class Comment(models.Model):
    post = models.ForeignKey(Post,on_delete=models.CASCADE,related_name='comments')
    author = models.ForeignKey(User,on_delete=models.CASCADE,related_name='comments')
    body = models.TextField()
    created = models.DateTimeField(default=timezone.now)
    like = models.ManyToManyField(User,related_name='liked_comments',blank=True)
    dislike = models.ManyToManyField(User,related_name='disliked_comments',blank=True)
    
    class Meta:
        ordering = ('created',)

    def __str__(self):
        return 'Comment by {} on {} at {}'.format(self.author,self.post,self.created)