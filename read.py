import pymongo
from xlrd import open_workbook

DATABASE = "ITSupportingWebApp"


client = pymongo.MongoClient()
db = client[DATABASE]

wb = open_workbook('subjects.xlsx')
values = []
subjects = []
for s in wb.sheets():
    #print 'Sheet:',s.name
    for row in range(1, s.nrows):
        col_names = s.row(0)
        values = []
        for name, col in zip(col_names, range(s.ncols)):
            value  = (s.cell(row,col).value)
            try : value = str(int(value))
            except : pass
            values.append(value)
        subjects.append({
            'picture': '',
            'title':values[0],
            'slug':values[1],
            'overview': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            'created_by_id': 1,
            'status':values[3],
            'num_credits':values[2],
            'category':values[4],
        })

for subject in subjects:
    db.elearning_subject.insert(subject)